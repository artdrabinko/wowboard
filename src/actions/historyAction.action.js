import { createAction } from "redux-actions";

import { historyCommandActionType as historyActionType } from "./types";

const historyInit = createAction(historyActionType.HISTORY_INIT);
const historyUndo = createAction(historyActionType.HISTORY_UNDO);
const historyRedo = createAction(historyActionType.HISTORY_REDO);
const historySnapshot = createAction(historyActionType.HISTORY_MAKE_SNAPSHOT);
const historyClear = createAction(historyActionType.HISTORY_CLEAR);

export default {
  historyInit,
  historyUndo,
  historyRedo,
  historySnapshot,
  historyClear,
};
