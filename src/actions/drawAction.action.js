import { createAction } from "redux-actions";

import { drawCommandActionType as drawAction } from "./types";

const clearDrawArena = createAction(drawAction.CLEAR_DRAW_ARENA);
const updateDrawSettings = createAction(drawAction.UPDATE_DRAW_SETTINGS);

const enableDrawTool = createAction(drawAction.ENABLE_DRAW_TOOL);
const disableDrawTool = createAction(drawAction.DISABLE_DRAW_TOOL);
const drawPoint = createAction(drawAction.DRAW_POINT);
const erasePoint = createAction(drawAction.ERASE_POINT);

export default {
  clearDrawArena,
  updateDrawSettings,

  enableDrawTool,
  disableDrawTool,
  drawPoint,
  erasePoint,
};
