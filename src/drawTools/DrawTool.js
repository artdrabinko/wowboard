class DrawTool {
  constructor(context, drawColor = "#111", lineWidth = 8) {
    this.context = context;

    this.defaultColor = drawColor;
    this.defaultLineWidth = lineWidth;
    this.points = [];
    this.isEnabled = false;
  }

  setLineStyle(settings) {
    const { drawColor, lineWidth } = settings;

    this.context.strokeStyle = drawColor ? drawColor : this.defaultColor;
    this.context.lineWidth = lineWidth ? lineWidth : this.defaultLineWidth;
    this.context.lineJoin = this.context.lineCap = "round";
  }

  enable() {
    this.isEnabled = true;
  }

  disable() {
    this.isEnabled = false;
    this.points.length = 0;
  }

  isActive() {
    return this.isEnabled;
  }

  setContext(context) {
    this.context = context;
  }

  clearContext() {
    this.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
  }

  midPointBtw(p1, p2) {
    return {
      x: p1.x + (p2.x - p1.x) / 2,
      y: p1.y + (p2.y - p1.y) / 2,
    };
  }

  draw(pointX, pointY) {
    if (!this.isEnabled) {
      return;
    }

    const ctx = this.context;

    this.points.push({ x: pointX, y: pointY });

    var p1 = this.points[0];
    var p2 = this.points[1];

    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);

    for (
      let pointIndex = 1, len = this.points.length;
      pointIndex < len;
      pointIndex++
    ) {
      if (len === 2) {
        break;
      }

      let midPoint = this.midPointBtw(p1, p2);
      ctx.quadraticCurveTo(p1.x, p1.y, midPoint.x, midPoint.y);
      p1 = this.points[pointIndex];
      p2 = this.points[pointIndex + 1];
    }

    ctx.lineTo(p1.x, p1.y);
    ctx.stroke();
  }
}

export default DrawTool;
