class DrawHistory {
  constructor(canvas, context) {
    this.redoList = [];
    this.undoList = [];

    this.canvas = canvas;
    this.context = context;
  }

  setCanvas(canvas) {
    this.canvas = canvas;
  }

  setContext(context) {
    this.context = context;
  }

  addHistorySnapshot() {
    this.saveState();
  }

  undo() {
    this.restoreState(this.undoList, this.redoList);
  }

  redo() {
    this.restoreState(this.redoList, this.undoList);
  }

  clear() {
    this.redoList = [];
    this.undoList = [];
  }

  saveState(list, keepRedo) {
    keepRedo = keepRedo || false;
    if (!keepRedo) {
      this.redoList = [];
    }

    (list || this.undoList).push(this.canvas.toDataURL());
  }

  restoreState(pop, push) {
    if (pop.length) {
      this.saveState(push, true);

      const restoreState = pop.pop();
      const img = document.createElement("img", { src: restoreState });
      img.src = restoreState;

      img.onload = () => {
        const canvasWidth = window.innerWidth;
        const canvasHeight = window.innerHeight;
        this.context.clearRect(0, 0, canvasWidth, canvasHeight);
        this.context.drawImage(
          img,
          0,
          0,
          canvasWidth,
          canvasHeight,
          0,
          0,
          canvasWidth,
          canvasHeight
        );
      };
    }
  }
}

export default DrawHistory;
