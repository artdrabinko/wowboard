import React from "react";
import { oneOf } from "prop-types";

import "./index.scss";

export const PanelToolHistoryButton = (props) => {
  const { type } = props;
  const baseStyle = `panel-tool-button panel-tool-button__history-${type}`;
  const seletedStyle = `${baseStyle}`;

  return <button className={seletedStyle} {...props}></button>;
};

PanelToolHistoryButton.defaultProps = {
  type: "undo",
};

PanelToolHistoryButton.propTypes = {
  type: oneOf(["undo", "redo"]),
};
