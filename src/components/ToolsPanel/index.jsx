import React, { useState } from "react";
import { connect } from "react-redux";
import { drawAction, historyAction } from "../../actions";
import {
  PanelToolPenButton,
  PanelToolErasePencilButton,
  PanelToolHistoryButton,
} from "../";

import "./index.scss";

const penColors = [
  {
    name: "black",
    hex: "#111",
  },
  {
    name: "red",
    hex: "#ff1b1b",
  },
  {
    name: "green",
    hex: "#099c14",
  },
  {
    name: "blue",
    hex: "#215fd2",
  },
];

const ToolsPanel = ({ updateDrawSettings, historyUndo, historyRedo }) => {
  const [selectedToolIndex, setSelectedTool] = useState(0);

  const handleSelectDrawTool = (selectedToolIndex, settings) => {
    setSelectedTool(selectedToolIndex);
    updateDrawSettings(settings);
  };

  const renderPens = () => {
    return penColors.map(({ name, hex }, toolIndex) => {
      return (
        <PanelToolPenButton
          key={toolIndex}
          color={name}
          selected={toolIndex === selectedToolIndex}
          onClick={() => {
            handleSelectDrawTool(toolIndex, { drawColor: hex });
          }}
        />
      );
    });
  };

  const renderEraseTool = () => {
    const eraseToolIndex = 1001;

    return (
      <PanelToolErasePencilButton
        key={eraseToolIndex}
        selected={eraseToolIndex === selectedToolIndex}
        onClick={() => {
          handleSelectDrawTool(eraseToolIndex, {
            drawColor: "white",
            lineWidth: 25,
          });
        }}
      />
    );
  };

  const renderDrawTools = () => {
    return [renderPens(), renderEraseTool()];
  };

  return (
    <aside className="Tools-Panel">
      {renderDrawTools()}
      <PanelToolHistoryButton type="undo" onClick={historyUndo} />
      <PanelToolHistoryButton type="redo" onClick={historyRedo} />
    </aside>
  );
};

const mapDispatchToProps = {
  updateDrawSettings: drawAction.updateDrawSettings,
  historyUndo: historyAction.historyUndo,
  historyRedo: historyAction.historyRedo,
};

export default connect(null, mapDispatchToProps)(ToolsPanel);
