import React from "react";
import settingsSrc from "../../assets/settings.svg";

import "./index.scss";

export const SettingsButton = (props) => {
  return (
    <button className="Settings-Button">
      <img
        className="Settings-Icon"
        width="22"
        height="22"
        src={settingsSrc}
        alt="set"
      />
    </button>
  );
};
