import React from "react";
import { bool } from "prop-types";

import "./index.scss"

export const PanelToolErasePencilButton = (props) => {
  const { selected } = props;

  const baseStyle = `panel-tool-button panel-tool-button__erase`;
  const seletedStyle = `${baseStyle} panel-tool-button__selected`;

  const styles = selected ? seletedStyle : baseStyle;

  return <button  className={styles} {...props}></button>;
};

PanelToolErasePencilButton.defaultProps = {
  selected: false
};

PanelToolErasePencilButton.propTypes = {
  selected: bool
};
