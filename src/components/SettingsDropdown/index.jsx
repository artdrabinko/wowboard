import React, { useState } from "react";
import { connect } from "react-redux";
import { SettingsButton, SettingsMenuItem, Divider } from "../";
import { historyAction, drawAction } from "../../actions";

import trashIconSrc from "../../assets/trash-icon.svg";
import aboutIconSrc from "../../assets/about-icon.svg";
import "./index.scss";

const SettingsDropdown = ({ historyClear, clearDrawArena }) => {
  const [isOpen, openMenu] = useState(false);

  return (
    <div className="Settings-Dropdown" onClick={() => openMenu(!isOpen)}>
      <SettingsButton />
      <aside
        className="Settings-Menu"
        style={{ display: isOpen ? "flex" : "none" }}
      >
        <SettingsMenuItem
          iconSrc={trashIconSrc}
          value="Clear canvas"
          onClick={() => {
            historyClear();
            clearDrawArena();
          }}
        />
        <Divider />
        <SettingsMenuItem iconSrc={aboutIconSrc} value="About" />
      </aside>
    </div>
  );
};

const mapDispatchToProps = {
  historyClear: historyAction.historyClear,
  clearDrawArena: drawAction.clearDrawArena,
};

export default connect(null, mapDispatchToProps)(SettingsDropdown);
