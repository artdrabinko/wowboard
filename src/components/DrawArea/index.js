import React, { useEffect } from "react";
import { connect } from "react-redux";
import { historyAction, drawAction } from "../../actions";

import "./index.scss";

const DrawArea = ({
  drawHistoryInit,
  makeHistorySnapshot,
  enableDrawTool,
  drawPoint,
  disableDrawTool,
}) => {
  const convasRef = React.createRef();

  const isTouchEvent = (event) => {
    return event.type.includes("touch");
  };

  const startDraw = (event) => {
    makeHistorySnapshot();
    enableDrawTool();
    drawing(event);
  };

  const drawing = (event) => {
    const canvas = event.currentTarget;

    let pointX = event.clientX;
    let pointY = event.clientY;

    if (isTouchEvent(event)) {
      pointX = event.touches[0].clientX;
      pointY = event.touches[0].clientY;
    }

    drawPoint({
      context: canvas.getContext("2d"),
      pointX,
      pointY,
    });
  };

  useEffect(() => {
    const canvas = convasRef.current;
    const context = canvas.getContext("2d");

    // @TODO find way to resize canvas element
    const resizeCanvas = () => {
      canvas.width = window.innerWidth;
      canvas.height = window.innerHeight;
    };

    window.addEventListener("resize", resizeCanvas, false);
    resizeCanvas();

    drawHistoryInit({
      canvas,
      context
    });
  });

  return (
    <div className="DrawArea-Container">
      <canvas
        ref={convasRef}
        className="App-DrawArea"
        height="480"
        width="720"
        onMouseDown={startDraw}
        onMouseMove={drawing}
        onMouseUp={disableDrawTool}
        onMouseOver={disableDrawTool}
        onTouchStart={startDraw}
        onTouchMove={drawing}
        onTouchEnd={disableDrawTool}
      />
    </div>
  );
};

const mapDispatchToProps = {
  drawHistoryInit: historyAction.historyInit,
  makeHistorySnapshot: historyAction.historySnapshot,

  enableDrawTool: drawAction.enableDrawTool,
  disableDrawTool: drawAction.disableDrawTool,
  drawPoint: drawAction.drawPoint,
};

export default connect(null, mapDispatchToProps)(DrawArea);
