import React from "react";
import { bool, oneOf } from "prop-types";

import "./index.scss"

export const colorClasses = {
  black: "pen-black",
  red: "pen-red",
  green: "pen-green",
  blue: "pen-blue",
};

export const PanelToolPenButton = (props) => {
  const { selected, color } = props;

  const baseStyle = `panel-tool-button ${colorClasses[color]}`;
  const seletedStyle = `${baseStyle} panel-tool-button__selected`;

  const styles = selected ? seletedStyle : baseStyle;

  return <button  className={styles} {...props}></button>;
};

PanelToolPenButton.defaultProps = {
  selected: false,
  color: "black"
};

PanelToolPenButton.propTypes = {
  selected: bool,
  color: oneOf(["black", "red", "green", "blue"]),
};
