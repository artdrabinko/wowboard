import React from "react";
import { string } from "prop-types";

import "./index.scss";

export const SettingsMenuItem = (props) => {
  const { iconSrc, value, onClick } = props;

  return (
    <button className="Settings-Menu_Item" onClick={onClick}>
      <img width="22" height="22" src={iconSrc} alt="icon" />
      <span>{value}</span>
    </button>
  );
};

SettingsMenuItem.propTypes = {
  iconSrc: string,
  value: string,
};
