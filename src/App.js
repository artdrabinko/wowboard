import React from "react";
import { SettingsDropdown, DrawArea, ToolsPanel } from "./components";

import "./index.css";

function App() {
  return (
    <div className="App-Container">
      <SettingsDropdown />
      <DrawArea />
      <ToolsPanel />
    </div>
  );
}

export default App;
