import { combineReducers } from "redux";

import drawCommandReducer from "./drawCommand.reducer";
import historyCommandReducer from "./historyCommand.reducer";

export default combineReducers({
  drawCommandReducer,
  historyCommandReducer,
});
