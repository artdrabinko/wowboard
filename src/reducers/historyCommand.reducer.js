import { historyCommandActionType as historyActionType } from "../actions/types";

import DrawHistory from "../drawHistory/DrawHistory";

const initialState = {
  historyInitialized: false,
};
const drawHistory = new DrawHistory();

const historyCommandReducer = (state = initialState, action) => {
  switch (action.type) {
    case historyActionType.HISTORY_INIT:
      const { canvas, context } = action.payload;

      drawHistory.setCanvas(canvas);
      drawHistory.setContext(context);

      return {
        historyInitialized: true,
      };

    case historyActionType.HISTORY_MAKE_SNAPSHOT:
      drawHistory.addHistorySnapshot();
      return state;

    case historyActionType.HISTORY_UNDO:
      drawHistory.undo();
      return state;

    case historyActionType.HISTORY_REDO:
      drawHistory.redo();
      return state;

    case historyActionType.HISTORY_CLEAR:
      drawHistory.clear();
      return state;

    default:
      return state;
  }
};

export default historyCommandReducer;
