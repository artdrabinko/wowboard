import {
  drawCommandActionType as drawActionType,
  historyCommandActionType as historyActionType,
} from "../actions/types";

import DrawTool from "../drawTools/DrawTool";

const initialState = {
  isDrawToolEnabled: false,
  drawColor: "#111",
  lineWidth: 5,
};

const drawTool = new DrawTool(
  null,
  initialState.drawColor,
  initialState.lineWidth
);

const drawCommandReducer = (state = initialState, action) => {
  switch (action.type) {
    case historyActionType.HISTORY_INIT:
      drawTool.setContext(action.payload.context);

      return state;

    case drawActionType.CLEAR_DRAW_ARENA:
      drawTool.clearContext();

      return state;

    case drawActionType.UPDATE_DRAW_SETTINGS:
      const { drawColor, lineWidth } = action.payload;

      const newLineWidth = lineWidth ? lineWidth : initialState.lineWidth;

      return {
        ...state,
        drawColor,
        lineWidth: newLineWidth,
      };

    case drawActionType.ENABLE_DRAW_TOOL:
      drawTool.enable();

      return {
        ...state,
        isDrawToolEnabled: true,
      };

    case drawActionType.DRAW_POINT:
      if (!state.isDrawToolEnabled && !drawTool.isActive()) {
        return state;
      }

      const { context, pointX, pointY } = action.payload;

      drawTool.setContext(context);
      drawTool.setLineStyle({
        drawColor: state.drawColor,
        lineWidth: state.lineWidth,
      });
      drawTool.draw(pointX, pointY);

      return state;

    case drawActionType.DISABLE_DRAW_TOOL:
      drawTool.disable();

      return {
        ...state,
        isDrawToolEnabled: false,
      };

    default:
      return state;
  }
};

export default drawCommandReducer;
